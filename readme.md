## 1. LINUX commands

#### LAN commands 



## 2. Docker commands 

show containers
`docker ps`

show running containers
`docker ps -a`

start a stopped docker container 
`docker start [container_name]`

stop a running container
`docker stop [container_name]`

restart a container
`docker restart [container_name]`

remove a container
`docker rm [container_name]`

remove a image
`docker rmi [container_name]`

inspect an image
`docker inspect [container_name]`


____________
## 3. MySQL

download and run images
`docker run -p3306:3306 --name mariadb -v /home/ciprian/Desktop/MyProjects/docker/datadir:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -d mariadb:10.1`

attach to container 
`docker exec -it ba724ba2753c mysql -uroot -proot`

change password
`ALTER USER 'root'@'localhost' IDENTIFIED BY '[newpassword]';`

### Running commands inside container
create new user
`CREATE USER 'ciprian'@localhost IDENTIFIED BY 'ciprian';`

show user from user table
`SELECT user FROM mysql.user;`

grand all privileges to the user 
`GRANT ALL PRIVILEGES ON *.* TO 'ciprian'@localhost IDENTIFIED BY 'ciprian'`

refresh privileges of users after modify it
`FLUSH PRIVILEGES;`

view all privileges of defined user
`SHOW GRANTS FOR 'ciprian'@localhost;`

delete user
`DROP user 'ciprian'@localhost;` 

return the host of db
`SELECT host FROM information_schema.processlist;`

show process list
`SHOW processlist;`

return the host name of db
`SELECT @@hostname;`

`SHOW DATABASES;`
`SHOW TABLES`
`USE  mysql;`

## 3. PostgreSQL

# Gitlab instructions

#### initiate a new repository
`git init`

#### change current branch
`git checkout branch_name`

#### create new branch
`git checkout -b branch_name`

#### add file to git stage
`git add path/to/file.txt`

#### add all files to git stage
`git add .`

#### commit added files from stage
`git commit -m 'my commit message here'`

#### update the local repository to match the content
`git pull`

#### first push on new branch
`git push -u origin branch_name`

#### normal push on branches
`git push`
#### or
`git push origin branch_name`

#### view git status
`git status`

#### view files changes
`git diff [path/to/file/or/directory]`

#### view logs history
`git logs`

#### show repository remote URL
`git remote -v`

#### delete remote URL
`git remote rm <name_of_remote>`

#### adding a remote url 
`git remote add <name> <url>`

#### clone a git repository
`git clone git@gitlab.com:raduic/demo-1.git [directory_name or . or nothing here]`


# SYMFONY

#### NOTES
create log messages in var/log/dev.log
`composer require logger`

Doctrine -> power of the db connection
`composer require doctrine`

#### Directory structure
```
|_
|_ src
    |_Controller
    |_Entity
    |_Repository
    -SoneServicesGenerator.php      \services
    -AnotherServicesGenerator.php       \services
|_
```

##### Connection to a database


